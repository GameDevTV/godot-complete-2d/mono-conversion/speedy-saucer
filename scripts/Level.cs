using Godot;
using System;

public partial class Level : Node2D
{
	private RigidBody2D _player;
	private PackedScene  _scene;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_scene = ResourceLoader.Load<PackedScene>("res://scenes/level_1.tscn");
		GD.Print("Level.cs>Level>_Ready(): ");

		Test();
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}

	public void Test()
	{
		GD.Print("Level.cs>Level>Test(): ");

		
	}

	public async void _on_maze_body_exited(Node2D player)
	{
		GD.Print("Level.cs>Level>_on_maze_body_exited(): ");

		await ToSignal(GetTree().CreateTimer(0.1), "timeout"); //Give some time to remove collision object.
		GetTree().ReloadCurrentScene();
	}
}