using Godot;
using System;

public partial class Player : RigidBody2D
{
	private RigidBody2D _player;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		GD.Print("Player.cs>Player>_Ready(): ");

		_player = GetNode<RigidBody2D>("/root/Level1/Player");

		Vector2 move = new Vector2(25f, 0f);

		_player.ApplyCentralImpulse(move);
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		//		GD.Print("Player.cs>Player>_Process(): " + Engine.GetFramesPerSecond());

		Vector2 move = new Vector2(0f, 0f);

		if (Input.IsActionPressed("move_right"))
		{
			move.X = 10f;
		}
		if (Input.IsActionPressed("move_left"))
		{
			move.X = -10f;
		}
		if (Input.IsActionPressed("move_up"))
		{
			move.Y = -10f;
		}
		if (Input.IsActionPressed("move_down"))
		{
			move.Y = 10f;
		}

		_player.ApplyCentralForce(move);

	}
}